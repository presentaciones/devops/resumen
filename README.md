# Title

## DevOps MinJUS

### Resumen

---

#### Atajos Presentación

* Zoom: Ctrl+Click
* Buscar: Ctrl+F
* Pantalla Completa: Alt+f
* Ir a la primera columna: Alt+H
* Ir a la última fila: Alt+J
* Ir a la primera fiila: Alt+K
* Ir a la última columna: Alt+L
* Vista Previa: Alt+O
* Exportar a PDF: /?print-pdf
* Ver notas: Alt+S

---


#### Usar

Con nodejs:
```bash
npm install
npm start
```

Con Docker:
```bash
# Construir y correr la imagen con docker:
docker build -t name-presentation:v1beta1 .
docker run -d -p 8000:8000 --name container-name name-presentation:v1beta1

# Construir y correr la imagen con docker compose:
docker compose -f docker-compose.dev.yaml up -d

# Correr la imagen directamente desde la registry
docker compose -f docker-compose.prod.yaml up -d
```

---

#### Plugins Integrados

Integrados:
- Markdown 

#### Plugins Externos
* [Menu](https://github.com/denehyg/reveal.js-menu)
* [Toolbar](https://github.com/denehyg/reveal.js-toolbar)
* [Mermaid](https://github.com/mermaid-js/mermaid)



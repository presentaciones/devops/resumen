# Próximamente

---V

## Normalización y CICD

Resta terminar de normalizar e implementar el CICD en todos los repositorios.

---V

## GitLab Pages

Integración de hosting de páginas estáticas a GitLab (en proceso)

---V

## Gestión de Secretos

Integración de ambiente de QA con **Vault**. (en proceso)

<small> Los secretos son datos sensibles y confidenciales que se utilizan para proteger y autenticar el acceso a sistemas, servicios, aplicaciones u otros recursos de infraestructura </small>

---V

## Aprovisionamiento

Automatización de configuraciones con **Ansible**. (en proceso)

<small> Ansible es una herramienta de automatización de código abierto utilizada para simplificar, automatizar y orquestar tareas de configuración, implementación y administración de sistemas, aplicaciones y recursos de infraestructura </small>

---V

## Métricas - logs - backups

- Swarmpit
- Grafana
- Prometheus
- Loki
- Velero

---V

## Entorno Productivo

Una vez terminada la migración de QA y realizadas las pruebas de rendimiento del cluster de QA, siguiendo la misma lógica, pasaremos a desplegar un nuevo cluster que compondrá el nuevo entorno **productivo**



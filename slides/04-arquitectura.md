# Arquitectura

---V

## Antiguamente teníamos este esquema

Cada VM podía tener uno o mas sistemas con la misma tecnología o bases de datos

<div class="mermaid">
flowchart TB
  subgraph "VM1 - PHP"
    app0["sistema1"]
    app1["sistema2"]
    app2["sistema3"]
  end

  subgraph "VM2"
    app3["sistema unico"]
  end

  subgraph "VM3"
    db1["mysql-sistema1"]
    db2["mysql-sistema2-3"]
  end

 subgraph "VM4"
    db3["sqlserver-sistema-unico"]
  end
</div>
---V

## Nuestra primer actualización

Consistió en dockerizar las aplicaciones, creando stacks completos en VMs
Creamos tres entornos individuales: `DEV`,`QA`,`PROD`

<div class="mermaid">
flowchart TB
  subgraph "PROD"
    Proxy00[Proxy]-.->app0-prod[app0]
    Proxy00[Proxy]-.->app1-prod[app1]
    Proxy00[Proxy]-.->appN-prod[appN]
  end
  subgraph "QA"
    Proxy01[Proxy]-.->app0-qa[app0]
    Proxy01[Proxy]-.->app1-qa[app1]
    Proxy01[Proxy]-.->appN-qa[appN]
  end
  subgraph "DEV"
    Proxy02[Proxy]-.->app0-dev[app0]
    Proxy02[Proxy]-.->app1-dev[app1]
    Proxy02[Proxy]-.->appN-dev[appN]
  end
</div>

---V

## En lo que estamos trabajando: **Clusters**

Empezamos con una prueba de concepto en servidores de DPSIT. (kubernetes y swarm).

<div class="mermaid">
graph TB
  subgraph "DPSIT"
    Load-Balancer-.->Nodo00
    Load-Balancer-.->Nodo01
    Load-Balancer-.->Nodo02
    subgraph "Swam"
      Nodo00-.-Nodo01
      Nodo00-.-Nodo02
      Nodo02-.-Nodo01
    end
  end
</div>

---V

## Cluster QA
Con el nuevo datacenter comenzamos a trabajar con un clúster swarm de ambiente bajo (QA)

<div class="mermaid">
graph LR
  subgraph "SERVERS MJUS"
    Load-Balancer-.-> manager
    subgraph manager
     direction TB
      manager-0[manager-0]
      manager-1
      manager-2
    end
    direction TB
    manager -.-> worker
    subgraph "worker"
        direction TB
      worker-0
      worker-1
      worker-2
    end
  end
</div>

- **Progresivamente se irán migrando todas las aplicaciones**

---V

## Impacto

- **Alta disponibilidad**: si un nodo falla, los contenedores se pueden reprogramar en otros nodos disponibles, manteniendo la disponibilidad del servicio.
- **Escalabilidad**: facilita la escalabilidad de las aplicaciones.
- **Balanceo de Carga**: permite distribuir las peticiones entre diferentes replicas
- **Actualizaciones Sin Tiempo de Inactividad**: garantiza que los servicios sigan disponibles durante el proceso de actualización.

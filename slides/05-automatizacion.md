# Automatización

---V

## CICD

- **Continuous Integration (CI)**: Cada vez que se realiza una nueva integración en el repositorio, se ejecutan tareas automatizadas.
- **Continuous Deployment (CD)**: Después de que se realizan la integración, la implementación continua (o entrega continua) permite automatizar el proceso de despliegue del software en un entorno de producción o de pruebas.

---V

## GitLab CI/CD

Es una herramienta de GitLab que permite aplicar el flujo CICD

Hasta ahora veníamos trabajando únicamente con el CI que generaba una imágen mediante la creación de un _tag_.

**Ahora contamos con el CICD completo**, es decir, genera la imágen y la deploya en el cluster de QA mencionado anteriormente.


---V

## GitLab Selfhosted

- Ventajas:
  - Eliminamos las restricciones que impone gitlab.com
  - Los datos están en MJUS.
  - Control total.
- Desventajas:
  - Deben realizarse todas las integraciones a la plataforma. Ej: Registry, Runners que permiten ejecutar GitLab CICD, etc.
  - Requiere mantenimiento.

---V

## Normalización - Ramas

Diseñamos un esquema de normalización basado en **[Gitflow](https://nvie.com/posts/a-successful-git-branching-model/)** para aplicarse a todos los repositorios que impacta directamente en el CICD.

<div class="mermaid">
%%{init: {'theme': 'base' } }%%
gitGraph
    commit tag:"v.1.0"
    branch qa
    checkout qa
    commit
    branch dev
    checkout dev
    commit
    commit
    checkout qa
    merge dev
    checkout dev
    commit
    checkout qa
    merge dev
    checkout main
    merge qa tag:"v.2.0"
</div>

<small> Gitflow es un modelo alternativo de creación de ramas en Git en el que se utilizan ramas de función y varias ramas principales. </small>

---V

## Normalización - Directorios

Repositorio
```
*
|
|- ...
|- devops/            --> dir. con lo necesario para el cicd
|- docker-compose.yml --> desarrollo
|- ...
```

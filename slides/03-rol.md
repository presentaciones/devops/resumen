# Conceptualizando roles

---V

## Equipo de Operaciones

También conocido como el equipo de operaciones de tecnología de la información (TI), es responsable de mantener y administrar la infraestructura de TI en una organización. Esto incluye servidores, redes, sistemas operativos, almacenamiento, seguridad, monitoreo y otros componentes tecnológicos necesarios para el funcionamiento de los sistemas.

---V

## Equipo de Operaciones (2)

Algunos de los roles que conforman éste equipo pueden ser:

- **SysAdmin**: administración y configuración de los sistemas.
- **NetAdmin**: gestión y mantenimiento de las redes.
- **Seguridad**: seguridad de los sistemas y los datos.
- **DBA**: gestión y mantenimiento de las bases de datos.
- **Almacenamiento**: gestión y mantenimiento de la infraestructura de almacenamiento.

---V

## SysAdmin vs Developer

Antes:

- **Desarrollador**: entrega de SW.
- **SysAdmin**: despliegue de SW.

El equipo SysAdmin no era capaz de responder con la rapidez necesaria las solicitudes de el equipo de desarrollo. Esta metodología de trabajo era ineficiente y además generaba fricciones entre ambos equipos.

---V

## Cultura DevOps

- Metodología de colaboración entre los equipos de desarrollo y de operaciones. El objetivo principal de DevOps es acelerar el desarrollo, las pruebas y la implementación de software, al tiempo que se garantiza la estabilidad, la confiabilidad y la seguridad del entorno de producción.

---V

## SRE

Surgió en Google, y es el **rol que aplica la Cultura DevOps**. SRE significa Site Reliability Engineering (Ingeniería de Confiabilidad de Sitios).

- El libro ["Site Reliability Engineering: How Google Runs Production Systems"](https://sre.google/books/), que documenta la metodología y las prácticas de SRE, fue publicado por Google en 2016.

Desde entonces, el concepto de SRE ha ganado popularidad y se ha convertido en una referencia en la industria de la tecnología.

---V

## Equipos MJUS

En el Ministerio tenemos:
- ...
- Desarrollo
  - ...
  - **Equipo DevOps (SRE)**
- Infraestructura
  - ...
  - Equipo SRE (Operaciones TI)

---V

## Nuestros pilares son:

1. Colaboración

2. Automatización

3. Entrega continua (CI/CD)

4. Monitoreo y retroalimentación constante

5. Mejora continua

Estos pilares trabajan en conjunto para promover una cultura ágil, colaborativa y orientada a resultados en el desarrollo y operaciones de software.

Note:

1. Colaboración
   Fomenta la comunicación y colaboración continua entre los equipos de desarrollo, operaciones y otras áreas relacionadas. Promover un enfoque conjunto hacia los objetivos comunes.

2. Automatización
   Se busca automatizar tanto como sea posible los procesos de desarrollo, pruebas, despliegue y monitoreo, para agilizar y estandarizar las tareas repetitivas y propensas a errores humanos.

3. Entrega continua (CI/CD)
   Se enfoca en realizar entregas frecuentes y pequeñas de software, utilizando prácticas como la integración continua (CI) y el deploy continuo (CD), lo que permite una rápida retroalimentación y adaptación a cambios.

4. Monitoreo y retroalimentación constante
    Se implementa un monitoreo continuo de las aplicaciones y sistemas en producción, para detectar problemas rápidamente y tomar acciones correctivas. La retroalimentación constante ayuda a mejorar el rendimiento y calidad del software.

5. Mejora continua
    Se fomenta una mentalidad de mejora constante, basada en la retroalimentación, el aprendizaje y la experimentación. Los equipos buscan identificar áreas de mejora y aplicar cambios incrementales para optimizar los procesos y resultados.

---V

<img src=images/devops-logo.webp height=400px/>


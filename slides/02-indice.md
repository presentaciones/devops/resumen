# Índice

---V
## [Conceptualizando Roles](#/2)
- [Equipo de Operaciones](#/2/1)
- [SysAdmin vs Developer](#/2/3)
- [Cultura DevOps](#/2/4)
- [SRE](#/2/5)
- [Ciclo DevOps](#/2/6)

---V
## [Arquitectura](#/3)
- [Ambientes](#/3/1)
- [Clusters](#/3/2)
- [Cluster QA](#/3/3)
- [Impacto](#/3/4)

---V
## [Automatización](#/4)
- [CICD](#/4/1)
- [GitLab CI/CD](#/4/2)
- [GitLab Selfhosted](#/4/3)
- [Normalización - Ramas](#/4/4)
- [Normalización - Directorios](#/4/5)

---V
## [Próximamente...](#/5)
- [Normalización y CICD](#/5/1)
- [GitLab Pages](#/5/2)
- [Gestión de secretos](#/5/3)
- [Aprovisionamiento](#/5/4)
- [Entorno productivo](#/5/5)

FROM node:lts-alpine3.17 as dev

LABEL maintainer="vccarp@gmail.com"

ARG ENVIRONMENT=prod 

WORKDIR /usr/src

COPY . .

RUN npm install -g gulp; \
    npm install

CMD ["npm", "start"];

# --

FROM dev as build

RUN npm run build

# --

FROM nginx as prod
WORKDIR /usr/share/nginx/html

COPY --from=build /usr/src/dist /usr/share/nginx/html/dist
COPY --from=build /usr/src/index.html /usr/share/nginx/html
COPY --from=build /usr/src/slides /usr/share/nginx/html/slides
COPY --from=build /usr/src/images /usr/share/nginx/html/images
COPY --from=build /usr/src/plugin /usr/share/nginx/html/plugin
